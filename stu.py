#定义一个学生列表
'''
stulist=[
	{'name':'zhangsan','age':20,'classid':'python03'},
	{'name':'lisi','age':23,'classid':'python04'},
	{'name':'Wzq','age':27,'classid':'python05'}
	]
#定义一个输出函数
def showstu(stulist):
	if len(stulist)==0:
		print('没有可输出的学生信息:')
	else:
		print('|{0:3}|{1:10}|{2:3}|{3:10}|'.format('sid','name','age','classid'))
		print('-'*43)
		for i in range(len(stulist)):
			print('|{0:3}|{1:10}|{2:3}|{3:10}|'.format(i+1,stulist[i]['name'],stulist[i]['age'],stulist[i]['classid']))

#定义一个继续函数
def c():
	while True:
		cc=input('按C键继续程序:')
		if cc=='c' or cc=='C':
			break
		else:
			print('输入错误，请重新输入:')
#从输出界面开始
while True:
	print('='*12,'学生信息管理系统','='*12)
	print('{0:3}{1:15}{2:15}'.format('','1.查看学生信息','2.添加学生信息'))
	print('{0:3}{1:15}{2:15}'.format('','3.删除学生信息','4.退出管理程序'))
	print('='*42)

	key=input('请输入对应的操作标识数字:')

	if key=='1':
		print('='*12,'查看学生信息','='*12)
		showstu(stulist)
		#这个函数在定义的时候就有形参，所以调用的时候必须要传入参数，否则报错
		c()
	elif key=='2':
		print('='*12,'添加学生信息','='*12)
		showstu(stulist)
		stu={}
		stu['name']=input('请输入学生姓名：')
		stu['age']=input('请输入学生年龄：')
		stu['classid']=input('请输入学生班级号：')
		stulist.append(stu)
		print('学生信息添加成功！')
		c()
	elif key=='3':
		print('='*12,'删除学生信息','='*12)
		d=input('请输入要删除的学生ID号：')
		del stulist[int(d)-1]
		showstu(stulist)
		c()
	elif key=='4':
		print('='*12,'退出管理程序','='*12)
		break
	else:

		print('='*12,'请输入正确字符','='*12)
		continue
		'''
import time

print(time.time())
print('test')